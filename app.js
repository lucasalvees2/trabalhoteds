const express = require ('express');
const mongoose = require('mongoose');
const requireDir = require('require-dir');
//const bodyParser = require('body-parser');


//Importação das Rotas
const produto = require('./routes/produto');

//Inicialização de componetes
const app = express();
app.use(express.json());

//Conexão com o Banco de Dados
mongoose.connect('mongodb://localhost:27017/produtos', { useNewUrlParser:true});

//Require-dir: importação dos models do projeto
require('./models/Produto');

//Chamada de Rotas
app.use('/produto', produto);


const PORT = process.env.PORT || 3002
app.listen(PORT);