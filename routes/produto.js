const express = require('express');

const router = express.Router();

const ProdutoController = require('../controllers/ProdutoController');

router.get('/produto', ProdutoController.index);
router.post('/produto', ProdutoController.novoProduto);
router.get('/produto/:id', ProdutoController.show);
router.put('/produto/:id', ProdutoController.update);
router.delete('/produto/:id', ProdutoController.destroy);

module.exports = router;